package fr.senjicorp.utils;

import static fr.senjicorp.utils.FabriqueMonnaie.fabriquerMonnaie;

import fr.senjicorp.monnaie.Piece;
import fr.senjicorp.monnaie.ENation;
import fr.senjicorp.monnaie.EPiece;

/**
 * @author Senji
 *
 */
public class FabriquePiece {

	private FabriquePiece() {}

	public static Piece fabriquerPiece(EPiece typePiece, ENation typeMonnaie) {
		Piece vRet = new Piece();
		vRet.setMonnaie(fabriquerMonnaie(typeMonnaie));
		vRet.setType(typePiece);
		return vRet;
	}

}
