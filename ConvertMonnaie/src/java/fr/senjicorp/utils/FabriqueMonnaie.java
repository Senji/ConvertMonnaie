package fr.senjicorp.utils;

import java.util.EnumMap;
import java.util.Map;

import fr.senjicorp.monnaie.Monnaie;
import fr.senjicorp.monnaie.ENation;

/**
 * @author Senji
 *
 */
public class FabriqueMonnaie {

	private static Map<ENation, Monnaie> cacheMonnaie = new EnumMap<>(ENation.class);

	private FabriqueMonnaie() {}

	public static Monnaie fabriquerMonnaie(ENation type) {
		Monnaie vRet = cacheMonnaie.get(type);
		if (vRet == null) {
			vRet = new Monnaie(type);
			cacheMonnaie.put(type, vRet);
		}
		return vRet;
	}
}
