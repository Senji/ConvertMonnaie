package fr.senjicorp.convertion;

/**
 * @author Senji
 *
 */
public enum EAction {
	CHANGE_NATION, CHANGE_PLATINE, MONNAIE, MONNAIE_NON_HUMAINE
}
