package fr.senjicorp.convertion;

/**
 * @author Senji
 *
 */
public class TauxConvertion {
	private double taux;
	private EAction typeAction;

	private TauxConvertion() {}

	public static TauxConvertion getTaux(EAction typeAction) {
		TauxConvertion vRet = new TauxConvertion();
		vRet.setTypeAction(typeAction);
		switch (typeAction) {
			case MONNAIE:
				vRet.setTaux(0.98);
				break;
			case CHANGE_PLATINE:
				vRet.setTaux(0.95);
				break;
			case CHANGE_NATION:
				vRet.setTaux(0.9);
				break;
			case MONNAIE_NON_HUMAINE:
				vRet.setTaux(0.75);
				break;
			default:
				break;
		}
		return vRet;
	}

	public double getTaux() {
		return this.taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public EAction getTypeAction() {
		return this.typeAction;
	}

	public void setTypeAction(EAction typeAction) {
		this.typeAction = typeAction;
	}
}
