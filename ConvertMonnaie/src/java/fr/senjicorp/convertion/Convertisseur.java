package fr.senjicorp.convertion;

import java.util.Iterator;

import fr.senjicorp.monnaie.Bourse;
import fr.senjicorp.monnaie.EPiece;
import fr.senjicorp.monnaie.Piece;
import fr.senjicorp.monnaie.Tas;
import fr.senjicorp.utils.FabriquePiece;

/**
 * @author Senji
 *
 */
public class Convertisseur {

	private Convertisseur() {}

	/**
	 * Permet de réaliser les actions de convertions voulues.
	 *
	 * @param piecesPossedees
	 * @param pieceVoulue
	 * @return
	 */
	public static Bourse convertir(final Tas piecesPossedees, final Piece pieceVoulue) {
		EAction vAction = getAction(piecesPossedees, pieceVoulue);

		if (vAction == null) {
			// Même pièce
			Bourse vRet = new Bourse();
			vRet.getPieces().add(piecesPossedees);
			return vRet;
		} else {
			Bourse vRet = new Bourse();
			boolean vIsConverti = false;
			switch (vAction) {
				case MONNAIE_NON_HUMAINE:
					// Pas encore géré
					return null;
				case CHANGE_NATION:
					vIsConverti = changeNation(piecesPossedees, pieceVoulue, vRet);
					if (vIsConverti) {
						gestionTauxChange(vRet, TauxConvertion.getTaux(EAction.CHANGE_NATION));
					}
					break;
				case CHANGE_PLATINE:
					vIsConverti = changeMonnaie(piecesPossedees, pieceVoulue, vRet);
					if (vIsConverti) {
						gestionTauxChange(vRet, TauxConvertion.getTaux(EAction.CHANGE_PLATINE));
					}
					break;
				case MONNAIE:
					vIsConverti = changeMonnaie(piecesPossedees, pieceVoulue, vRet);
					if (vIsConverti) {
						gestionTauxChange(vRet, TauxConvertion.getTaux(EAction.MONNAIE));
					}
					break;
				default:
					return null;
			}
			return vRet;
		}
	}

	/**
	 * Permet de savoir quel type d'action est défini pour la convertion donnée
	 * 
	 * @param piecesPossedees
	 * @param pieceVoulue
	 * @return
	 */
	public static EAction getAction(Tas piecesPossedees, Piece pieceVoulue) {
		if (piecesPossedees.getPiece().getMonnaie().getTypeMonnaie()
				.equals(pieceVoulue.getMonnaie().getTypeMonnaie())) {
			// Même monnaie
			if (piecesPossedees.getPiece().getType().equals(pieceVoulue.getType())) {
				// Même pièce
				return null;
			} else {
				if (EPiece.PLATINE.equals(pieceVoulue.getType())) {
					// Changement en pièces de platine
					return EAction.CHANGE_PLATINE;
				} else {
					// Cahnge de type de pièces
					return EAction.MONNAIE;
				}
			}
		} else {
			// Monnaie différente
			return EAction.CHANGE_NATION;
		}
	}

	private static boolean changeMonnaie(Tas piecesPossedees, Piece pieceVoulue, Bourse bourse) {
		boolean vIsConverti = false;
		// Récupération du rapport de convertion
		int vConvertion = piecesPossedees.getPiece().getMonnaie().getConvertion(piecesPossedees.getPiece(),
				pieceVoulue);
		int vNbPieces = piecesPossedees.getNbPieces();

		if (vConvertion > 0) {
			// Cas où le nombre pièce est augmenté
			bourse.getPieces().add(new Tas(vNbPieces * vConvertion, pieceVoulue));
			vIsConverti = true;
		} else if (vConvertion < 0) {
			// Cas où le nombre de pièce diminu
			int vReste = vNbPieces % vConvertion;
			if (vReste == 0) {
				// Cas où la division est un nombre juste
				bourse.getPieces().add(new Tas(vNbPieces / vConvertion, pieceVoulue));
				vIsConverti = true;
			} else {
				// Cas où la division n'est pas un nombre juste
				if ((vNbPieces / vConvertion) != 0) {
					// S'il y a bien des pièces qui sont converties.
					bourse.getPieces().add(new Tas(vNbPieces / vConvertion, pieceVoulue));
					vIsConverti = true;
				}
				bourse.getPieces().add(new Tas(vReste, piecesPossedees.getPiece()));
			}
		}

		return vIsConverti;

	}

	private static boolean changeNation(Tas piecesPossedees, Piece pieceVoulue, Bourse bourse) {
		boolean vIsConverti = false;

		Bourse vTemp = new Bourse();
		// On passe on pièce de platine de la bonne monnaie
		if (!EPiece.PLATINE.equals(piecesPossedees.getPiece().getType())) {
			vIsConverti = changeMonnaie(piecesPossedees,
					FabriquePiece.fabriquerPiece(EPiece.PLATINE, pieceVoulue.getMonnaie().getTypeMonnaie()), vTemp);
		}
		// Puis on passe en pièce voulue dans la bonne monnaie si on a des pièces de
		// platine
		if (vIsConverti) {
			Tas vTas = null;

			Iterator<Tas> iterator = vTemp.getPieces().iterator();
			while (iterator.hasNext() && vTas == null) {
				vTas = iterator.next();
				if (!!vTas.getPiece().getMonnaie().getTypeMonnaie().equals(pieceVoulue.getMonnaie().getTypeMonnaie())) {
					// Si le tas n'est pas de la bonne monnaie
					vTas = null;
				}
			}

			// Récupération du rapport de convertion
			int vConvertion = vTas.getPiece().getMonnaie().getConvertion(piecesPossedees.getPiece(), pieceVoulue);
			int vNbPieces = vTas.getNbPieces();

			if (vConvertion > 0) {
				// Cas où le nombre pièce est augmenté
				bourse.getPieces().add(new Tas(vNbPieces * vConvertion, pieceVoulue));
				vIsConverti = true;
			} else if (vConvertion < 0) {
				// Cas où le nombre de pièce diminu
				int vReste = vNbPieces % vConvertion;
				if (vReste == 0) {
					// Cas où la division est un nombre juste
					bourse.getPieces().add(new Tas(vNbPieces / vConvertion, pieceVoulue));
					vIsConverti = true;
				} else {
					// Cas où la division n'est pas un nombre juste
					if ((vNbPieces / vConvertion) != 0) {
						// S'il y a bien des pièces qui sont converties.
						bourse.getPieces().add(new Tas(vNbPieces / vConvertion, pieceVoulue));
						vIsConverti = true;
					}
					bourse.getPieces().add(new Tas(vReste, vTas.getPiece()));
				}
			}
		}

		return vIsConverti;

	}

	private static void gestionTauxChange(Bourse vRet, TauxConvertion tauxConvertion) {
		// TODO Auto-generated method stub

	}

}
