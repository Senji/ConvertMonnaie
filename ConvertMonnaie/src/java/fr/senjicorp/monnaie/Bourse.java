package fr.senjicorp.monnaie;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Senji
 *
 */
public class Bourse {
	private List<Tas> pieces;

	public Bourse() {
		this.pieces = new ArrayList<Tas>();
	}

	public Bourse(List<Tas> pieces) {
		this.pieces = pieces;
	}

	public List<Tas> getPieces() {
		return this.pieces;
	}

	public void setPieces(List<Tas> pieces) {
		this.pieces = pieces;
	}
}
