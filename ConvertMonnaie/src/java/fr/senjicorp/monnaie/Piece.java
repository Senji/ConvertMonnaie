package fr.senjicorp.monnaie;

/**
 * @author Senji
 *
 */
public class Piece {
	private EPiece type;
	private Monnaie monnaie;

	public String getNom() {
		return this.monnaie.getNomPiece(this.type);
	}

	public EPiece getType() {
		return this.type;
	}

	public void setType(EPiece type) {
		this.type = type;
	}

	public Monnaie getMonnaie() {
		return this.monnaie;
	}

	public void setMonnaie(Monnaie monnaie) {
		this.monnaie = monnaie;
	}
}
