package fr.senjicorp.monnaie;

/**
 * @author Senji
 *
 */
public enum EPiece {
	CUIVRE, ARGENT, OR, PLATINE
}
