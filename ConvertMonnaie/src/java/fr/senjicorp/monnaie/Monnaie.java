package fr.senjicorp.monnaie;

/**
 * @author Senji
 *
 */
public class Monnaie {
	private ENation typeMonnaie;

	public Monnaie() {

	}

	public Monnaie(ENation type) {
		this.typeMonnaie = type;
	}

	public int getConvertion(Piece pieceOrigine, Piece pieceDestination) {
		return this.getConvertion(pieceOrigine.getType(), pieceDestination.getType());
	}

	public int getConvertion(EPiece typePieceOrigine, EPiece typePieceDestination) {
		switch (this.typeMonnaie) {
			case CONGLOMERAT:
				return this.getConvertionConglomerat(typePieceOrigine, typePieceDestination);
			case EMPIRE:
				return this.getConvertionEmpire(typePieceOrigine, typePieceDestination);
			case NOMADE:
				return this.getConvertionNomade(typePieceOrigine, typePieceDestination);
			case NORDIQUE:
				return this.getConvertionNordique(typePieceOrigine, typePieceDestination);
			case ROYAUME:
				return this.getConvertionRoyaume(typePieceOrigine, typePieceDestination);
			default:
				return 0;
		}
	}

	private int getConvertionRoyaume(EPiece typePieceOrigine, EPiece typePieceDestination) {
		int vDiff = typePieceOrigine.ordinal() - typePieceDestination.ordinal();
		if (vDiff == 0) {
			return 1;
		} else if (vDiff > 0) {
			return (vDiff * 20);
		} else {
			return (-(vDiff * 20));
		}
	}

	private int getConvertionNordique(EPiece typePieceOrigine, EPiece typePieceDestination) {
		// [1,10,20,100]
		int vRet = 0;

		int vDiff = typePieceOrigine.ordinal() - typePieceDestination.ordinal();
		if (vDiff == 0) {
			vRet = 1;
		} else if (vDiff > 0) {
			switch (typePieceOrigine) {
				case PLATINE:
					switch (typePieceDestination) {
						case OR:
							vRet = 10;
							break;
						case ARGENT:
							// ->Or * ->Argent
							vRet = (10 * 20);
							break;
						case CUIVRE:
							// ->Or * ->Argent * ->Cuivre
							vRet = (10 * 20 * 100);
							break;
						default:
							break;
					}
					break;
				case OR:
					switch (typePieceDestination) {
						case ARGENT:
							vRet = 20;
							break;
						case CUIVRE:
							// ->Argent * ->Cuivre
							vRet = (20 * 100);
							break;
						default:
							break;
					}
					break;
				case ARGENT:
					if (EPiece.CUIVRE.equals(typePieceDestination)) {
						vRet = 100;
					}
					break;
				default:
					break;
			}
		} else {
			vRet = (-this.getConvertionNordique(typePieceDestination, typePieceOrigine));
		}
		return vRet;
	}

	private int getConvertionNomade(EPiece typePieceOrigine, EPiece typePieceDestination) {
		// [1,100,20,100]
		int vRet = 0;

		int vDiff = typePieceOrigine.ordinal() - typePieceDestination.ordinal();
		if (vDiff == 0) {
			vRet = 1;
		} else if (vDiff > 0) {

			switch (typePieceOrigine) {
				case PLATINE:
					switch (typePieceDestination) {
						case OR:
							vRet = 100;
							break;
						case ARGENT:
							// ->Or * ->Argent
							vRet = (100 * 20);
							break;
						case CUIVRE:
							// ->Or * ->Argent * ->Cuivre
							vRet = (100 * 20 * 100);
							break;
						default:
							break;
					}
					break;
				case OR:
					switch (typePieceDestination) {
						case ARGENT:
							vRet = 20;
							break;
						case CUIVRE:
							// ->Argent * ->Cuivre
							vRet = (20 * 100);
							break;
						default:
							break;
					}
					break;
				case ARGENT:
					if (EPiece.CUIVRE.equals(typePieceDestination)) {
						vRet = 100;
					}
					break;
				default:
					break;
			}
		} else {
			vRet = (-this.getConvertionNomade(typePieceDestination, typePieceOrigine));
		}
		return vRet;
	}

	private int getConvertionEmpire(EPiece typePieceOrigine, EPiece typePieceDestination) {
		int vDiff = typePieceOrigine.ordinal() - typePieceDestination.ordinal();
		if (vDiff == 0) {
			return 1;
		} else if (vDiff > 0) {
			return (vDiff * 100);
		} else {
			return (-(vDiff * 100));
		}
	}

	private int getConvertionConglomerat(EPiece typePieceOrigine, EPiece typePieceDestination) {
		int vDiff = typePieceOrigine.ordinal() - typePieceDestination.ordinal();
		if (vDiff == 0) {
			return 1;
		} else if (vDiff > 0) {
			return (vDiff * 50);
		} else {
			return (-(vDiff * 50));
		}
	}

	public String getNomPiece(EPiece typePiece) {
		switch (this.typeMonnaie) {
			case CONGLOMERAT:
				return this.getNomPieceConglomerat(typePiece);
			case EMPIRE:
				return this.getNomPieceEmpire(typePiece);
			case NOMADE:
				return this.getNomPieceNomade(typePiece);
			case NORDIQUE:
				return this.getNomPieceNordique(typePiece);
			case ROYAUME:
				return this.getNomPieceRoyaume(typePiece);
			default:
				return "";
		}
	}

	private String getNomPieceRoyaume(EPiece typePiece) {
		switch (typePiece) {
			case PLATINE:
				return "Couronne de Platine";
			case OR:
				return "Couronne d'Or";
			case ARGENT:
				return "Couronne d'Argent";
			case CUIVRE:
				return "Couronne de Cuivre";
			default:
				return null;
		}
	}

	private String getNomPieceNordique(EPiece typePiece) {
		switch (typePiece) {
			case PLATINE:
				return "Fé";
			case OR:
				return "Gull";
			case ARGENT:
				return "Pennigar";
			case CUIVRE:
				return "Kopar";
			default:
				return null;
		}
	}

	private String getNomPieceNomade(EPiece typePiece) {
		switch (typePiece) {
			case PLATINE:
				return "Riyal";
			case OR:
				return "Dirhams";
			case ARGENT:
				return "Qhirshs";
			case CUIVRE:
				return "Halalas";
			default:
				return null;
		}
	}

	private String getNomPieceEmpire(EPiece typePiece) {
		switch (typePiece) {
			case PLATINE:
				return "Tribun";
			case OR:
				return "Centurion";
			case ARGENT:
				return "Optione";
			case CUIVRE:
				return "Percuarii";
			default:
				return null;
		}
	}

	private String getNomPieceConglomerat(EPiece typePiece) {
		switch (typePiece) {
			case PLATINE:
				return "Gallion";
			case OR:
				return "Caraque";
			case ARGENT:
				return "Ketch";
			case CUIVRE:
				return "Sloop";
			default:
				return null;
		}
	}

	public ENation getTypeMonnaie() {
		return this.typeMonnaie;
	}

	public void setTypeMonnaie(ENation typeMonnaie) {
		this.typeMonnaie = typeMonnaie;
	}
}
