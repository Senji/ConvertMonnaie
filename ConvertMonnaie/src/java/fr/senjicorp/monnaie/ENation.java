package fr.senjicorp.monnaie;

/**
 * @author Senji
 *
 */
public enum ENation {
	ROYAUME, EMPIRE, CONGLOMERAT, NOMADE, NORDIQUE
}
