package fr.senjicorp.monnaie;

/**
 * @author Senji
 *
 */
public class Tas {
	private int nbPieces;
	private Piece piece;

	public Tas(int nbPieces, Piece piece) {
		this.nbPieces = nbPieces;
		this.piece = piece;
	}

	public int getNbPieces() {
		return this.nbPieces;
	}

	public void setNbPieces(int nbPieces) {
		this.nbPieces = nbPieces;
	}

	public Piece getPiece() {
		return this.piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
	}

}
