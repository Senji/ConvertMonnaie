package fr.senjicorp.monnaie;

import static fr.senjicorp.monnaie.ENation.CONGLOMERAT;
import static fr.senjicorp.monnaie.ENation.EMPIRE;
import static fr.senjicorp.monnaie.ENation.NOMADE;
import static fr.senjicorp.monnaie.ENation.NORDIQUE;
import static fr.senjicorp.monnaie.ENation.ROYAUME;
import static fr.senjicorp.monnaie.EPiece.ARGENT;
import static fr.senjicorp.monnaie.EPiece.CUIVRE;
import static fr.senjicorp.monnaie.EPiece.OR;
import static fr.senjicorp.monnaie.EPiece.PLATINE;
import static fr.senjicorp.utils.FabriqueMonnaie.fabriquerMonnaie;

import org.junit.Assert;
import org.junit.Test;

public class MonnaieTest {

	@Test
	public void testGetConvertion() throws Exception {
		for (int i = 0; i < ENation.values().length; i++) {
			switch (ENation.values()[i]) {
				case CONGLOMERAT:
					this.testGetConvertionConglomerat();
					break;
				case EMPIRE:
					this.testGetConvertionEmpire();
					break;
				case NOMADE:
					this.testGetConvertionNomade();
					break;
				case NORDIQUE:
					this.testGetConvertionNordique();
					break;
				case ROYAUME:
					this.testGetConvertionRoyaume();
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetConvertionRoyaume() {
		Monnaie monnaie = fabriquerMonnaie(ROYAUME);

		for (int i = 0; i < EPiece.values().length; i++) {
			for (int j = 0; j < EPiece.values().length; j++) {
				if (i == j) {
					// Si même type de pièce alors 1
					Assert.assertEquals(1, monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else if (i > j) {
					// Si la destination est inférieure à l'origine
					Assert.assertEquals((i - j) * 20,
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else {
					Assert.assertEquals(-((i - j) * 20),
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				}
			}
		}
	}

	@Test
	public void testGetConvertionNordique() {
		Monnaie monnaie = fabriquerMonnaie(NORDIQUE);

		Assert.assertEquals(1, monnaie.getConvertion(PLATINE, PLATINE));
		Assert.assertEquals(10, monnaie.getConvertion(PLATINE, OR));
		Assert.assertEquals(10 * 20, monnaie.getConvertion(PLATINE, ARGENT));
		Assert.assertEquals(10 * 20 * 100, monnaie.getConvertion(PLATINE, CUIVRE));

		Assert.assertEquals(-10, monnaie.getConvertion(OR, PLATINE));
		Assert.assertEquals(1, monnaie.getConvertion(OR, OR));
		Assert.assertEquals(20, monnaie.getConvertion(OR, ARGENT));
		Assert.assertEquals(20 * 100, monnaie.getConvertion(OR, CUIVRE));

		Assert.assertEquals(-(10 * 20), monnaie.getConvertion(ARGENT, PLATINE));
		Assert.assertEquals(-20, monnaie.getConvertion(ARGENT, OR));
		Assert.assertEquals(1, monnaie.getConvertion(ARGENT, ARGENT));
		Assert.assertEquals(100, monnaie.getConvertion(ARGENT, CUIVRE));

		Assert.assertEquals(-(10 * 20 * 100), monnaie.getConvertion(CUIVRE, PLATINE));
		Assert.assertEquals(-(20 * 100), monnaie.getConvertion(CUIVRE, OR));
		Assert.assertEquals(-100, monnaie.getConvertion(CUIVRE, ARGENT));
		Assert.assertEquals(1, monnaie.getConvertion(CUIVRE, CUIVRE));
	}

	@Test
	public void testGetConvertionNomade() {
		Monnaie monnaie = fabriquerMonnaie(NOMADE);

		Assert.assertEquals(1, monnaie.getConvertion(PLATINE, PLATINE));
		Assert.assertEquals(100, monnaie.getConvertion(PLATINE, OR));
		Assert.assertEquals(100 * 20, monnaie.getConvertion(PLATINE, ARGENT));
		Assert.assertEquals(100 * 20 * 100, monnaie.getConvertion(PLATINE, CUIVRE));

		Assert.assertEquals(-100, monnaie.getConvertion(OR, PLATINE));
		Assert.assertEquals(1, monnaie.getConvertion(OR, OR));
		Assert.assertEquals(20, monnaie.getConvertion(OR, ARGENT));
		Assert.assertEquals(20 * 100, monnaie.getConvertion(OR, CUIVRE));

		Assert.assertEquals(-(100 * 20), monnaie.getConvertion(ARGENT, PLATINE));
		Assert.assertEquals(-20, monnaie.getConvertion(ARGENT, OR));
		Assert.assertEquals(1, monnaie.getConvertion(ARGENT, ARGENT));
		Assert.assertEquals(100, monnaie.getConvertion(ARGENT, CUIVRE));

		Assert.assertEquals(-(100 * 20 * 100), monnaie.getConvertion(CUIVRE, PLATINE));
		Assert.assertEquals(-(20 * 100), monnaie.getConvertion(CUIVRE, OR));
		Assert.assertEquals(-100, monnaie.getConvertion(CUIVRE, ARGENT));
		Assert.assertEquals(1, monnaie.getConvertion(CUIVRE, CUIVRE));
	}

	@Test
	public void testGetConvertionEmpire() {
		Monnaie monnaie = fabriquerMonnaie(EMPIRE);

		for (int i = 0; i < EPiece.values().length; i++) {
			for (int j = 0; j < EPiece.values().length; j++) {
				if (i == j) {
					// Si même type de pièce alors 1
					Assert.assertEquals(1, monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else if (i > j) {
					// Si la destination est inférieure à l'origine
					Assert.assertEquals((i - j) * 100,
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else {
					Assert.assertEquals(-((i - j) * 100),
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				}
			}
		}
	}

	@Test
	public void testGetConvertionConglomerat() {
		Monnaie monnaie = fabriquerMonnaie(CONGLOMERAT);

		for (int i = 0; i < EPiece.values().length; i++) {
			for (int j = 0; j < EPiece.values().length; j++) {
				if (i == j) {
					// Si même type de pièce alors 1
					Assert.assertEquals(1, monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else if (i > j) {
					// Si la destination est inférieure à l'origine
					Assert.assertEquals((i - j) * 50,
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				} else {
					Assert.assertEquals(-((i - j) * 50),
							monnaie.getConvertion(EPiece.values()[i], EPiece.values()[j]));
				}
			}
		}
	}

	@Test
	public void testGetNomPiece() throws Exception {
		for (int i = 0; i < ENation.values().length; i++) {
			switch (ENation.values()[i]) {
				case CONGLOMERAT:
					this.testGetNomPieceConglomerat();
					break;
				case EMPIRE:
					this.testGetNomPieceEmpire();
					break;
				case NOMADE:
					this.testGetNomPieceNomade();
					break;
				case NORDIQUE:
					this.testGetNomPieceNordique();
					break;
				case ROYAUME:
					this.testGetNomPieceRoyaume();
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetNomPieceRoyaume() {
		Monnaie monnaie = fabriquerMonnaie(ROYAUME);

		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					Assert.assertEquals("Couronne de Platine", monnaie.getNomPiece(PLATINE));
					break;
				case OR:
					Assert.assertEquals("Couronne d'Or", monnaie.getNomPiece(OR));
					break;
				case ARGENT:
					Assert.assertEquals("Couronne d'Argent", monnaie.getNomPiece(ARGENT));
					break;
				case CUIVRE:
					Assert.assertEquals("Couronne de Cuivre", monnaie.getNomPiece(CUIVRE));
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetNomPieceNordique() {
		Monnaie monnaie = fabriquerMonnaie(NORDIQUE);

		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					Assert.assertEquals("Fé", monnaie.getNomPiece(PLATINE));
					break;
				case OR:
					Assert.assertEquals("Gull", monnaie.getNomPiece(OR));
					break;
				case ARGENT:
					Assert.assertEquals("Pennigar", monnaie.getNomPiece(ARGENT));
					break;
				case CUIVRE:
					Assert.assertEquals("Kopar", monnaie.getNomPiece(CUIVRE));
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetNomPieceNomade() {
		Monnaie monnaie = fabriquerMonnaie(NOMADE);

		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					Assert.assertEquals("Riyal", monnaie.getNomPiece(PLATINE));
					break;
				case OR:
					Assert.assertEquals("Dirhams", monnaie.getNomPiece(OR));
					break;
				case ARGENT:
					Assert.assertEquals("Qhirshs", monnaie.getNomPiece(ARGENT));
					break;
				case CUIVRE:
					Assert.assertEquals("Halalas", monnaie.getNomPiece(CUIVRE));
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetNomPieceEmpire() {
		Monnaie monnaie = fabriquerMonnaie(EMPIRE);

		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					Assert.assertEquals("Tribun", monnaie.getNomPiece(PLATINE));
					break;
				case OR:
					Assert.assertEquals("Centurion", monnaie.getNomPiece(OR));
					break;
				case ARGENT:
					Assert.assertEquals("Optione", monnaie.getNomPiece(ARGENT));
					break;
				case CUIVRE:
					Assert.assertEquals("Percuarii", monnaie.getNomPiece(CUIVRE));
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

	@Test
	public void testGetNomPieceConglomerat() {
		Monnaie monnaie = fabriquerMonnaie(CONGLOMERAT);

		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					Assert.assertEquals("Gallion", monnaie.getNomPiece(PLATINE));
					break;
				case OR:
					Assert.assertEquals("Caraque", monnaie.getNomPiece(OR));
					break;
				case ARGENT:
					Assert.assertEquals("Ketch", monnaie.getNomPiece(ARGENT));
					break;
				case CUIVRE:
					Assert.assertEquals("Sloop", monnaie.getNomPiece(CUIVRE));
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

}
