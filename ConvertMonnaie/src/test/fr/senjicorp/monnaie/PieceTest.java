package fr.senjicorp.monnaie;

import static fr.senjicorp.monnaie.EPiece.ARGENT;
import static fr.senjicorp.monnaie.EPiece.CUIVRE;
import static fr.senjicorp.monnaie.EPiece.OR;
import static fr.senjicorp.monnaie.EPiece.PLATINE;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class PieceTest {

	@Test
	public void testGetNom() throws Exception {
		Monnaie vMock = Mockito.mock(Monnaie.class);
		Piece vPiece = new Piece();
		vPiece.setMonnaie(vMock);
		for (int i = 0; i < EPiece.values().length; i++) {
			switch (EPiece.values()[i]) {
				case PLATINE:
					vPiece.setType(PLATINE);
					vPiece.getNom();
					verify(vMock, times(1)).getNomPiece(vPiece.getType());
					break;
				case OR:
					vPiece.setType(OR);
					vPiece.setMonnaie(vMock);
					vPiece.getNom();
					verify(vMock, times(1)).getNomPiece(vPiece.getType());
					break;
				case ARGENT:
					vPiece.setType(ARGENT);
					vPiece.setMonnaie(vMock);
					vPiece.getNom();
					verify(vMock, times(1)).getNomPiece(vPiece.getType());
					break;
				case CUIVRE:
					vPiece.setType(CUIVRE);
					vPiece.setMonnaie(vMock);
					vPiece.getNom();
					verify(vMock, times(1)).getNomPiece(vPiece.getType());
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

}
