package fr.senjicorp.utils;

import static fr.senjicorp.monnaie.ENation.CONGLOMERAT;
import static fr.senjicorp.monnaie.ENation.EMPIRE;
import static fr.senjicorp.monnaie.ENation.NOMADE;
import static fr.senjicorp.monnaie.ENation.NORDIQUE;
import static fr.senjicorp.monnaie.ENation.ROYAUME;
import static fr.senjicorp.utils.FabriqueMonnaie.fabriquerMonnaie;

import org.junit.Assert;
import org.junit.Test;

import fr.senjicorp.monnaie.Monnaie;
import fr.senjicorp.monnaie.ENation;

public class FabriqueMonnaieTest {

	@Test
	public void testFabriquerMonnaie() throws Exception {
		Monnaie monnaie = null;
		for (int i = 0; i < ENation.values().length; i++) {
			switch (ENation.values()[i]) {
				case CONGLOMERAT:
					monnaie = fabriquerMonnaie(CONGLOMERAT);
					Assert.assertEquals(monnaie.getTypeMonnaie(), CONGLOMERAT);
					break;
				case EMPIRE:
					monnaie = fabriquerMonnaie(EMPIRE);
					Assert.assertEquals(monnaie.getTypeMonnaie(), EMPIRE);
					break;
				case NOMADE:
					monnaie = fabriquerMonnaie(NOMADE);
					Assert.assertEquals(monnaie.getTypeMonnaie(), NOMADE);
					break;
				case NORDIQUE:
					monnaie = fabriquerMonnaie(NORDIQUE);
					Assert.assertEquals(monnaie.getTypeMonnaie(), NORDIQUE);
					break;
				case ROYAUME:
					monnaie = fabriquerMonnaie(ROYAUME);
					Assert.assertEquals(monnaie.getTypeMonnaie(), ROYAUME);
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

}
