package fr.senjicorp.utils;

import static fr.senjicorp.utils.FabriquePiece.fabriquerPiece;

import org.junit.Assert;
import org.junit.Test;

import fr.senjicorp.monnaie.Piece;
import fr.senjicorp.monnaie.ENation;
import fr.senjicorp.monnaie.EPiece;

public class FabriquePieceTest {

	@Test
	public void testFabriquerPiece() throws Exception {
		for (int i = 0; i < EPiece.values().length; i++) {
			for (int j = 0; j < ENation.values().length; j++) {
				Piece vPiece = fabriquerPiece(EPiece.values()[i], ENation.values()[j]);
				Assert.assertEquals(EPiece.values()[i], vPiece.getType());
				Assert.assertEquals(ENation.values()[j], vPiece.getMonnaie().getTypeMonnaie());
			}
		}
	}

}
