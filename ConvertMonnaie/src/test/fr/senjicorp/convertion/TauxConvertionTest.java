package fr.senjicorp.convertion;

import static fr.senjicorp.convertion.EAction.CHANGE_NATION;
import static fr.senjicorp.convertion.EAction.CHANGE_PLATINE;
import static fr.senjicorp.convertion.EAction.MONNAIE;
import static fr.senjicorp.convertion.EAction.MONNAIE_NON_HUMAINE;

import org.junit.Assert;
import org.junit.Test;

public class TauxConvertionTest {

	@Test
	public void testGetTauxTypeAction() throws Exception {
		TauxConvertion vTemp = null;
		for (int i = 0; i < EAction.values().length; i++) {
			switch (EAction.values()[i]) {
				case CHANGE_NATION:
					vTemp = TauxConvertion.getTaux(CHANGE_NATION);
					vTemp.getTypeAction().equals(CHANGE_NATION);
					Assert.assertEquals(1 - (10.0 / 100.0), vTemp.getTaux(), 0);
					break;
				case CHANGE_PLATINE:
					vTemp = TauxConvertion.getTaux(CHANGE_PLATINE);
					vTemp.getTypeAction().equals(CHANGE_PLATINE);
					Assert.assertEquals(1 - (5.0 / 100.0), vTemp.getTaux(), 0);
					break;
				case MONNAIE:
					vTemp = TauxConvertion.getTaux(MONNAIE);
					vTemp.getTypeAction().equals(MONNAIE);
					Assert.assertEquals(1 - (2.0 / 100.0), vTemp.getTaux(), 0);
					break;
				case MONNAIE_NON_HUMAINE:
					vTemp = TauxConvertion.getTaux(MONNAIE_NON_HUMAINE);
					vTemp.getTypeAction().equals(MONNAIE_NON_HUMAINE);
					Assert.assertEquals(1 - (25.0 / 100.0), vTemp.getTaux(), 0);
					break;
				default:
					Assert.fail();
					break;
			}
		}
	}

}
