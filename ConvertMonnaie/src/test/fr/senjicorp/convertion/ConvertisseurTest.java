package fr.senjicorp.convertion;

import static fr.senjicorp.convertion.Convertisseur.getAction;
import static fr.senjicorp.utils.FabriquePiece.fabriquerPiece;

import org.junit.Assert;
import org.junit.Test;

import fr.senjicorp.monnaie.ENation;
import fr.senjicorp.monnaie.EPiece;
import fr.senjicorp.monnaie.Piece;
import fr.senjicorp.monnaie.Tas;

public class ConvertisseurTest {

	@Test
	public void testGetAction() throws Exception {
		// Initialisation, pas d'action
		Piece vOrigine = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		Tas vTas = new Tas(1, vOrigine);
		Piece vDestination = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		EAction vAction = getAction(vTas, vDestination);
		Assert.assertNull(vAction);

		// Changement de type de pièce
		vOrigine = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		vTas = new Tas(1, vOrigine);
		vDestination = fabriquerPiece(EPiece.ARGENT, ENation.CONGLOMERAT);
		vAction = getAction(vTas, vDestination);
		Assert.assertEquals(EAction.MONNAIE, vAction);

		// Changement en pièce de platine
		vOrigine = fabriquerPiece(EPiece.CUIVRE, ENation.CONGLOMERAT);
		vTas = new Tas(1, vOrigine);
		vDestination = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		vAction = getAction(vTas, vDestination);
		Assert.assertEquals(EAction.CHANGE_PLATINE, vAction);

		// Changement de nation, même pièce
		vOrigine = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		vTas = new Tas(1, vOrigine);
		vDestination = fabriquerPiece(EPiece.PLATINE, ENation.EMPIRE);
		vAction = getAction(vTas, vDestination);
		Assert.assertEquals(EAction.CHANGE_NATION, vAction);

		// Changement de nation et changement de pièce
		vOrigine = fabriquerPiece(EPiece.PLATINE, ENation.CONGLOMERAT);
		vTas = new Tas(1, vOrigine);
		vDestination = fabriquerPiece(EPiece.OR, ENation.NOMADE);
		vAction = getAction(vTas, vDestination);
		Assert.assertEquals(EAction.CHANGE_NATION, vAction);
	}

}
